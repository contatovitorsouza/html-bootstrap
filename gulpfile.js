const { watch, series, src, dest } = require('gulp');
const server    = require('browser-sync').create();
const sass      = require('gulp-sass');
const del       = require('del');

const paths = {
    scripts: {
        src: 'src/scripts/*.js',
        dest: 'dist/scripts/'
    },
    styles: {
        src: 'src/styles/*.scss',
        dest: 'dist/styles/'
    }
}

const clean = () => del(['dist']);
const watcher = () =>   watch(paths.scripts.src, series(scripts, reload))
                        watch(paths.styles.src, series(styles))
                        watch('./index.html', reload);

// Start static server
function browserSync(done) {
    server.init({
        server: {
            baseDir: "./"
        }
    });
    done();
};

function reload(done) {
    server.reload();
    done();
}

function scripts() {
    return src(paths.scripts.src)
        //in case you need to build scripts
        .pipe(dest(paths.scripts.dest));
}

function styles() {
    return src(paths.styles.src)
        .pipe(sass({
            includePaths: ['node_modules']
        }).on('error', sass.logError))
        .pipe(dest(paths.styles.dest))
        .pipe(server.stream());
}

exports.default = series(clean, scripts, styles, browserSync, watcher);

